<?php

namespace  Webdecero\Slide\Manager\Controllers;

//Providers
//Models
//Helpers and Class
use Webdecero\Base\Manager\Controllers\DataTableController;

class SlideDataTableController extends DataTableController {

    protected $collectionName = 'slide';
    
    protected $searchable = [
        'query' => [
            '_id' => 'contains',
            'titulo' => 'contains',
           
           
        ],
        'estatus' => 'equal',
        'locale' => 'equal',
        
        
    ];
    
    
    protected $fieldsDetails = [
        '_id' => 'ID',
        'titulo' => 'Título',
       
        'status' => 'status',
        'imagen' => 'Imagen',
        
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    protected $fieldsDetailsExcel = [
       '_id' => 'ID',
        'titulo' => 'Título',
       
        'status' => 'status',
        'imagen' => 'Imagen',
        
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {
 
        $record = parent::formatRecord($field, $item);


        if ($field == 'imagen' && isset($item[$field])) {

            $record = "<img class='thumb' src=" . $item[$field] . " >";
        } else if ($field == 'url_details') {

            $record = route('manager.slide.dataTable', ['id' => $item['_id']]);
        } else if ($field == 'opciones') {


            $record = "<a href=" . route('manager.slide.edit', ['id' => $item['_id'],'type' => $item['type']]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";

            $record .= "<a href=" . route('manager.slide.destroy', ['id' => $item['_id']]) . "  class='text-center center-block borrar'  data-toggle='tooltip' title='Eliminar'  > "
                    . "<i class='fa fa-trash-o'></i> "
                    . "</a>";
        }

        return $record;
    }

}
