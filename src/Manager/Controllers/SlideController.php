<?php

namespace Webdecero\Slide\Manager\Controllers;

//Providers
use Validator;
use Auth;
//Models
use Webdecero\Slide\Manager\Models\Slide;
//Helpers and Class
use Illuminate\Http\Request;
use Webdecero\Base\Manager\Controllers\ManagerController;
use Webdecero\Base\Manager\Traits\DynamicInputs;
use Webdecero\Base\Manager\Facades\Utilities;

class SlideController extends ManagerController {

    use DynamicInputs;

    private $arrayLocaleNative = [];
    protected $configPages = '';

    public function __construct() {

        parent::__construct();

        $this->configPages = config('manager.slide');
        
        
        
        foreach (\LaravelLocalization::getSupportedLocales() as $key => $value) {
            $this->arrayLocaleNative[$key] = $value['native'];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $this->data['user'] = Auth::user();
//		$this->data['categorias'] = Slide::getCategories();

        return view('baseViews::slidePanel', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {


        $this->data['user'] = Auth::user();
        $this->data['dataPage'] = [];


        $type = request('type', 'index');
        $configPage = $this->configPages[$type];


        $configPage['sections']['locale']['options'] = $this->arrayLocaleNative;

       // dd($this->data)
        $processData = $this->processPageUI($configPage, $this->data);

        return view('baseViews::slideAddForm', $processData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $input = $request->all();


        $rules = array(
            'titulo' => array('required', 'unique:slide,titulo'),
            'imagen' => array('required'),
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {


            $slide = new Slide;

            $this->_storeSlide($request, $slide);



            return redirect()->route('manager.slide.index')->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
        $slide = Slide::findOrFail($id);

        $isValid = Utilities::idMongoValid($id);

        if (!$isValid || !isset($slide->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }



        $this->data['user'] = Auth::user();

        $this->data['slide'] = $slide;


        $this->data['dataPage'] = $slide->toArray();

        $type = request('type', 'index');
        $configPage = $this->configPages[$type];



        $configPage['sections']['locale']['options'] = $this->arrayLocaleNative;


        $processData = $this->processPageUI($configPage, $this->data);

        return view('baseViews::slideEditForm', $processData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = $request->all();


        $rules = array(
            'titulo' => array('required'),
            'imagen' => array('required')
        );

        $input['id'] = $id;

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $isValid = Utilities::idMongoValid($input['id']);
            $slide = Slide::find($input['id']);

            if (!$isValid || !isset($slide->id)) {
                return redirect()->route('manager.slide.index')->with([
                            'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
                ]);
            }


            $this->_storeSlide($request, $slide);


            return redirect()->route('manager.slide.index')->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $isValid = Utilities::idMongoValid($id);
        $slide = Slide::findOrFail($id);

        if (!$isValid || !isset($slide->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }


        $slide->delete();

        return redirect()->route('manager.slide.index')->with([
                    'mensaje' => trans('baseLang::mensajes.operacion.correcta'),
        ]);
    }

    private function _storeSlide(Request $request, $slide) {

        $input = $request->all();

        $slide->fill($input);

        $type = request('type', 'index');
        $sections = $this->configPages[$type]['sections'];


        $slide = $this->dynamicFillSave($slide, $sections, $input);



        $slide->save();

        return $slide;
    }

}
