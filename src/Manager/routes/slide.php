<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//sliders
Route::post('slide/dataTable/{id?}/{excel?}', array('as' => 'manager.slide.dataTable', 'uses' => 'SlideDataTableController@dataTable'));
Route::get('slide/{id}/delete', ['as' => 'manager.slide.destroy', 'uses' => 'SlideController@destroy']);
Route::resource('slide', 'SlideController', [
    'except' => [
        'show', 'destroy'
    ],
    'names' => [
        'index' => 'manager.slide.index',
        'create' => 'manager.slide.create',
        'store' => 'manager.slide.store',
        'show' => 'manager.slide.show',
        'edit' => 'manager.slide.edit',
        'update' => 'manager.slide.update',
//        'destroy' => 'manager.slide.destroy',
    ]
]);
