@extends('baseViews::layouts.dataTable')


@section('sectionAction', 'Tabla')
@section('sectionName', 'Slider')
@section('formAction', route('manager.slide.dataTable'))


@section('inputs')
<div class="form-group">
    <label for="status">Estatus</label> <br>

    <select class="reloadTable" name="status">
        <option value="">Ninguno</option>
        <option value="true">Publicado</option>
        <option value="false">No Publicado</option>


    </select>
</div>

<div class="form-group">
    <label for="locale">Idioma</label> <br>

    <select class="reloadTable" name="locale">
        <option value="">Ninguno</option>
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
        <option value="{{ $localeCode }}">{{ $properties['native'] }}</option>
        @endforeach


    </select>
</div>



<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">

    <div class="btn-group" role="group" aria-label="...">

        <a class="btn btn-success" href="{{ route('manager.slide.create',['type'=> 'Principal'])}}" >
            <i class="fa fa-plus-circle"></i> Nuevo Slider principal
        </a>
        
         <a class="btn btn-success" href="{{ route('manager.slide.create',['type'=> 'Logros'])}}" >
            <i class="fa fa-plus-circle"></i> Nuevo Slider Logros
        </a>

    </div>
    <div class="btn-group" role="group" aria-label="...">
        <button type="submit"class="btn btn-success " >

            <i class="fa fa-file-excel-o"></i> EXPORT EXCEL

        </button>
    </div>
</div>

@endsection


@section('dataTableColums')


<th data-details="true" ></th>


<th data-name="imagen">
    <strong>Imagen</strong>
</th>

<th data-name="titulo" data-orderable="true"  >
    <strong>Título</strong>
</th>

<th data-name="locale">
    <strong>Idioma</strong>
</th>


<th data-name="type" data-orderable="true">
    <strong>Tipo</strong>
</th>

<th data-name="orden" data-orderable="true" data-order="asc">
    <strong>orden</strong>
</th>
<th data-name="status" data-orderable="true">
    <strong>Estatus</strong>
</th>

<th data-name="created_at"><strong>Fecha Alta</strong></th>

<th data-name="opciones">
    <strong>Opciones</strong>
</th>




@endsection
