@extends($layout)


@section('sectionAction', 'Editar')
@section('sectionName', $sectionName)
@section('formAction', route('manager.slide.update',[$slide->id]))

@section('inputs')
{{ method_field('PUT') }}

{!! $htmlInputs !!}

@endsection





