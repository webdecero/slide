<?php

namespace Webdecero\Slide;

use Illuminate\Support\ServiceProvider;
use RuntimeException;
use Illuminate\Support\Facades\Route;

//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class SlideServiceProvider extends ServiceProvider {

    
    
    private $configSlide = __DIR__ . '/../config/manager/slide.php';
    
    private $folderViews = __DIR__ . '/Manager/resources/views';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

       
//        Publishes Configuración Base
//        php artisan vendor:publish --provider="Webdecero\Slide\SlideServiceProvider" --tag=config

        $this->publishes([
            $this->configSlide => config_path('manager/slide.php'),
            
                ], 'config');

//        Registra Views
        $this->loadViewsFrom($this->folderViews, 'baseViews');

//ROUTE Slide 
        $config = $this->app['config']->get('manager.base.manager.route', []);

        if (empty($config)) {
            throw new RuntimeException('No se enecontro la configuración Base para ruta');
        }

        $config['namespace'] = 'Webdecero\Slide\Manager\Controllers';
        
        $originalMiddleware =$config['middleware'];

 
        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/slide.php');
        });
        

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        
        
        

        $this->mergeConfigFrom($this->configSlide, 'manager.slide');
        



//        $this->app->make('Webdecero\Slide\Manager\Controllers\SlideController');
    }

}
