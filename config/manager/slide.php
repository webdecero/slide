<?php

$pages = [];

$pages['index'] = [
    'isSeo' => FALSE,
    'sectionName' => 'Slide Principal',
    'sections' => [
        'type' => [
            'element' => 'inputHidden',
            
            'value' => 'index',
        ],
        'imagen' => [
            'element' => 'imageResize',
            
            'label' => 'Slide principal',
            'required' => true,
            'w' => '1280',
            'h' => '720',
            'required' => true,
            'resize' => 'widen',
            'path' => 'img/slide',
            'ratio' => '16 / 9',
        ],
        'titulo' => [
            'element' => 'inputText',
            
            'label' => 'Título',
            'required' => true,
        ],
        'contenido' => [
            'element' => 'inputTextarea',
            
            'label' => 'Contenido',
        ],
        'locale' => [
            'element' => 'inputSelect',
            
            'label' => 'Idioma',
            'required' => true,
        ],
        
        'orden' => [
            'element' => 'inputText',
            
            'label' => 'Orden',
            'required' => true,
        ],
         'status' => [
            'element' => 'inputSelect',
            
            'label' => 'Estatus',
            'required' => true,
             'options' => [
                 'true' =>'publicado',
                 'false' =>'No publicado'
             ],
             'helper'=>'Si el estatus es no publicado el artículo no estara visible dentro del sitio'
        ],
        
        
    ]
];





return $pages;

